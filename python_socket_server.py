# coding=utf-8
import socket
import select
import sys
import threading
import json
import mysql.connector as mariadb
import hashlib
import os
import time
import re
import shutil

class Server:
	
	def __init__(self):
		self.host = "127.0.0.1"
		self.port = 5566
		self.threads = []
		self.backlog = 5
		self.size = 2048

	def start_socket(self):
		try:
			self.server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			self.server_sock.bind( (self.host,self.port) )
			self.server_sock.listen(self.backlog)
		except :
			
			print("sock start error")

	def run(self):
		self.start_socket()
		inputs = [self.server_sock, sys.stdin]
		running = 1
		while running:
			read_ready,write_ready,error_ready = select.select(inputs,[],[])

			for fd in read_ready:

				if fd == self.server_sock:
					arg_host,arg_port = self.server_sock.accept()
					cli = Client(arg_host,arg_port)
					cli.start()
					self.threads.append(cli)
				elif fd == sys.stdin:
					junk_msg = sys.stdin.readline()
					running = 0
		
		self.server.close()
		for c in threads:
			c.join()

class Client(threading.Thread):
	
	def __init__( self, client_sock, client_address ) :
		threading.Thread.__init__(self)
		self.client_sock = client_sock
		self.client_address = client_address
		self.size = 2048

		try:
		    self.mariadb_conn = mariadb.connect(host='127.0.0.1',user='python_server', password='np2016final', database='np2016final',charset='utf8')
		    self.cursor = self.mariadb_conn.cursor(buffered=True)

		except Exception as e:
			print("%s",e)

	def calcSHA1(self, filename):	
		BLOCKSIZE = 65536
		hasher = hashlib.sha1()
		with open(filename, 'rb') as afile:
			buf = afile.read(BLOCKSIZE)
			while len(buf) > 0:
				hasher.update(buf)
				buf = afile.read(BLOCKSIZE)
			SHA1_value = hasher.hexdigest()
		#print("SHA1 = " + SHA1_value)

		return SHA1_value

	def download_from_server(self, filename):
		
		try:
			file_size_int = os.path.getsize(filename)
			file_size_str = str(file_size_int)
		except Exception as e:
			print("file now found")
			return
		resp_dict = dict()
		resp_dict['type'] = "Command"
		resp_dict['command'] = "download"
		resp_dict['filename'] = filename
		resp_dict['file_size'] = file_size_str
		#print("sending file size = " + file_size_str)
		#self.client_sock.send(file_size_str.encode('utf-8'))
		SHA1_value = self.calcSHA1(filename)
		resp_dict['file_sha1'] = SHA1_value
		#print("sending file SHA1 = " + SHA1_value)
		#self.client_sock.send(SHA1_value.encode('utf-8'))
		resp_json = json.dumps(resp_dict)
		self.client_sock.sendall(resp_json.encode('utf-8'))

		time.sleep(1)
		

		send_size = 0
		with open (filename, "rb") as file:
			while(send_size < file_size_int):
				if(file_size_int-send_size < self.size):
					file_data = file.read(file_size_int - send_size)
					send_size = file_size_int
				else:
					file_data = file.read(self.size)
					send_size += self.size
				self.client_sock.send(file_data)
			print("file send over")


	def upload_to_server(self, filename,file_size_int,SHA1_value):
		
		recv_size = 0
		with open (filename, "wb") as file:
			while(recv_size < file_size_int):
				if(file_size_int-recv_size < self.size):
					file_data = self.client_sock.recv(file_size_int-recv_size)
					recv_size = file_size_int
				else:
					file_data = self.client_sock.recv(self.size)
					recv_size += self.size
				file.write(file_data)
			print("Recv success")

		receiving_file_size_int = os.path.getsize(filename)
		#receiving_file_size_str = str(receiving_file_size_int)
		#print("receiving file size = ",receiving_file_size_int)
		receiving_SHA1_value = self.calcSHA1(filename)
		#print("receiving SHA1 = " + receiving_SHA1_value)
		if(receiving_file_size_int == file_size_int and receiving_SHA1_value == SHA1_value):
			print("The data received is correct!")

			
	def run(self):
		running = 1
		name_passwd_pattern = re.compile("[a-zA-Z0-9_]{1,50}")

		while running:
			json_data = b''
			json_data += self.client_sock.recv(self.size)

			if not json_data:
				self.client_sock.close()
				running = 0

			try:
				data = json.loads(json_data.decode('utf-8'))
			except Exception as e:
				print("json error %s",e)

			if data["command"] == "Register" :
				username = data["username"]
				username = username.lower()
				passwd = data["passwd"]

				
				try:
					regexp_username = name_passwd_pattern.match(username)
					regexp_password = name_passwd_pattern.match(passwd)
					
					if regexp_username is not None and regexp_password is not None:
						self.cursor.execute("SELECT * FROM user_list WHERE username = %s",(username,))
						result = self.cursor.fetchone() 

					if regexp_password is None or regexp_username is None or result is not None:
						#print("repeated username")
						resp_dict = dict()
						resp_dict['type'] = "Message"
						resp_dict['content'] = "Register Fail.(can't use this username)"
						resp_json = json.dumps(resp_dict)
						self.client_sock.sendall(resp_json.encode('utf-8'))
						continue


					self.cursor.execute("INSERT INTO user_list (username,password) VALUES (%s,%s)",(username,passwd))
					self.mariadb_conn.commit()
					userpath = "./users/" + username
					if not os.path.isdir(userpath)
						os.mkdir(userpath,0o777)
					sql = "CREATE TABLE " + username + "_file_list (serial int primary key auto_increment, filename varchar(32) NOT NULL, checksum char(40) NOT NULL)"
					self.cursor.execute(sql)
					self.mariadb_conn.commit()

					resp_dict = dict()
					resp_dict['type'] = "Message"
					resp_dict['content'] = "Register OK!"
					resp_json = json.dumps(resp_dict)
					self.client_sock.sendall(resp_json.encode('utf-8'))
				except Exception as e:
					print("%s",e)

				continue

			elif data["command"] == "Login":
				username = data["username"]
				username = username.lower()
				passwd = data["passwd"]

				regexp_username = name_passwd_pattern.match(username)
				regexp_password = name_passwd_pattern.match(passwd)
				
				if regexp_password is not None and regexp_username is not None :
					try:
						#sql = "SELECT * FROM user_list WHERE username = \'" + username + "\' AND password = \'" + passwd + "\'"
						sql = "SELECT* FROM user_list WHERE username = \'"+ username +"\' AND password = " + passwd
						self.cursor.execute(sql)
						self.mariadb_conn.commit()

					except Exception as e:
						print("%s",e)

					result = self.cursor.fetchone()
				if regexp_password is not None and regexp_username is not None and result is not None:
					resp_dict = dict()
					resp_dict['type'] = "Message"
					resp_dict['content'] = "Login OK!"
					resp_dict['username'] = username
					#self.download_from_server("test.jpg")
					resp_json = json.dumps(resp_dict)
					self.client_sock.sendall(resp_json.encode('utf-8'))

					userpath = "./users/" + username

					if not os.path.isdir(userpath):
						os.mkdir(userpath, 0o777)
					#self.download_from_server("test.jpg")
				else:
					resp_dict = dict()
					resp_dict['type'] = "Message"
					resp_dict['content'] = "Login Fail. Please check again"
					resp_json = json.dumps(resp_dict)
					self.client_sock.sendall(resp_json.encode('utf-8'))
				continue

			elif data["command"] == "Download" :
				filename = data["filename"]
				self.download_from_server(filename)

			elif data["command"] == "Upload" :
				print("command =  upload")
				filename = data["filename"]
				print("filename = ",filename)
				file_size_int = int(data["file_size"])
				print("file_size = ",file_size_int)
				file_sha1 = data["file_sha1"]
				print("file_sha1 = ",file_sha1)
				self.upload_to_server(filename,file_size_int,file_sha1)

			elif data["command"] == "NewFile":
				filename = data["filename"]
				print("filename",filename)
				resp_dict['type'] = "Command"
				resp_dict['command'] = "upload"
				resp_dict['filename'] = filename
				resp_json = json.dumps(resp_dict)
				self.client_sock.sendall(resp_json.encode('utf-8'))

			elif data["command"] == "NewDir":
				dirname = data["dirname"]
				os.mkdir(dirname,0o777)

			elif data["command"] == "DelFile":
				filename = data["filename"]
				try:
					os.remove(filename)
				except Exception as e:
					print(e)

			elif data["command"] == "DelDir":
				dirname = data["dirname"]
				try:
					shutil.rmtree(dirname, ignore_errors = True, onerror=None)
				except Exception as e:
					print(e)
				#need to remove all relative file column from database
			elif data["command"] == "ModFile":
				filename = data['filename']
				resp_dict = dict()
				resp_dict['type'] = "Command"
				resp_dict['command'] = "upload"
				resp_dict['filename'] = filename
				resp_json = json.dumps(resp_dict)
				self.client_sock.sendall(resp_json.encode('utf-8'))

			elif data["command"] == "Move":
				oldname = data['oldname']
				newname = data['newname']

				shutil.move(oldname,newname)

			else :
				print("other instr")
				continue


			
			

if __name__ == "__main__":
	s = Server()
	s.run()
	


