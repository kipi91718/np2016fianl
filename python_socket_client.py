# coding=utf-8
import socket
import sys
import json
import threading
import hashlib
import os
import time
from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer


class Recv_Msg_From_Server(threading.Thread):
	def __init__(self, arg,mutex):
		threading.Thread.__init__(self)
		self.server_sock = arg
		self.size = 2048
		self.sock_output_mutex = mutex


	def calc_SHA1(self, filename):	
		BLOCKSIZE = 65536
		hasher = hashlib.sha1()
		#print("filename is calc sha1 = ",filename)
		with open(filename, 'rb') as afile:
			buf = afile.read(BLOCKSIZE)
			while len(buf) > 0:
				hasher.update(buf)
				buf = afile.read(BLOCKSIZE)
			SHA1_value = hasher.hexdigest()
		#print("SHA1 = " + SHA1_value)

		return SHA1_value


	def download_from_server(self, filename,file_size_int,SHA1_value):
		
		recv_size = 0
		with open (filename, "wb") as file:
			while(recv_size < file_size_int):
				if(file_size_int-recv_size < self.size):
					file_data = self.server_sock.recv(file_size_int-recv_size)
					recv_size = file_size_int
				else:
					file_data = self.server_sock.recv(self.size)
					recv_size += self.size
				file.write(file_data)
			print("Recv success")

		receiving_file_size_int = os.path.getsize(filename)
		#receiving_file_size_str = str(receiving_file_size_int)
		#print("receiving file size = ",receiving_file_size_int)
		receiving_SHA1_value = self.calc_SHA1(filename)
		#print("receiving SHA1 = " + receiving_SHA1_value)
		if(receiving_file_size_int == file_size_int and receiving_SHA1_value == SHA1_value):
			print("The data received is correct!")


	def upload_to_server(self,filename):
		#print("filename in upload_to_server = " , filename)
		if not os.path.isfile(filename):
			return
		try:
			file_size_int = os.path.getsize(filename)
			#print("file_size = ",file_size_int)
			

		except Exception as e:
			print("file now found")
			return
		
		SHA1_value = self.calc_SHA1(filename)
		#print("SHA1_value = ",SHA1_value)
		file_size_str = str(file_size_int)

		upload_dict = dict()
		upload_dict['command'] = "Upload"
		upload_dict['filename'] = filename
		upload_dict['file_size'] = file_size_str
		upload_dict['file_sha1'] = SHA1_value

		upload_json = json.dumps(upload_dict)
		self.sock_output_mutex.acquire()
		self.server_sock.sendall(upload_json.encode('utf-8'))

		time.sleep(1)

		send_size = 0

		with open(filename,"rb") as file:
			while (send_size < file_size_int):
				if(file_size_int - send_size < self.size):
					file_data = file.read(file_size_int - send_size)
					send_size = file_size_int
				else:
					file_data = file.read(self.size)
					send_size += self.size

				self.server_sock.sendall(file_data)
		self.sock_output_mutex.release()
		print(filename,"upload finished")



	def run(self):
		running = 1

		while running:
			json_recvline = b''
			json_recvline += self.server_sock.recv(self.size)
			try:
				recvline = json.loads(json_recvline.decode('utf-8'))
			except Exception as e:
				print("json error %s",e)
			if recvline['type'] == "Message":
				print("\nServer Message : ",recvline['content'])
				if recvline['content'] == "Login OK!":
					username = recvline['username']
					userpath = "./users/" + username

					if not os.path.isdir(userpath):
						os.mkdir(userpath)

			elif recvline['type'] == "Command":
				if recvline['command'] == "download":
					filename = recvline['filename']
					file_size_int = int(recvline['file_size'])
					SHA1_value = recvline['file_sha1']
					self.download_from_server(filename,file_size_int,SHA1_value)

				elif recvline["command"] == "upload":
					filename = recvline['filename']
					
					self.upload_to_server(filename)

				
			#recvline = self.server_sock.recv(self.size)
			#print(recvline.decode('utf-8'))


class FileHandler(FileSystemEventHandler):
	def __init__(self,server_sock,mutex):
		super().__init__()
		self.server_sock = server_sock
		self.sock_output_mutex = mutex

	def on_created(self,event):
		if not event.is_directory:
			filename = event.src_path
			newfile_dict = dict()
			newfile_dict['command'] = "NewFile"
			newfile_dict['filename'] = filename
			newfile_json = json.dumps(newfile_dict)
			self.sock_output_mutex.acquire()
			self.server_sock.sendall(newfile_json.encode('utf-8'))
			self.sock_output_mutex.release()
		else:
			dirname = event.src_path
			newdir_dict = dict()
			newdir_dict['command'] = "NewDir"
			newdir_dict['dirname'] = dirname
			newdir_json = json.dumps(newdir_dict)
			self.sock_output_mutex.acquire()
			self.server_sock.sendall(newdir_json.encode('utf-8'))
			self.sock_output_mutex.release()

	def on_deleted(self,event):
		if not event.is_directory:
			filename = event.src_path
			delfile_dict = dict()
			delfile_dict['command'] = "DelFile"
			delfile_dict['filename'] = filename
			delfile_json = json.dumps(delfile_dict)
			self.sock_output_mutex.acquire()
			self.server_sock.sendall(delfile_json.encode('utf-8'))
			self.sock_output_mutex.release()
		else:
			dirname = event.src_path
			deldir_dict = dict()
			deldir_dict['command'] = "DelDir"
			deldir_dict['dirname'] = dirname
			deldir_json = json.dumps(deldir_dict)
			self.sock_output_mutex.acquire()
			self.server_sock.sendall(deldir_json.encode('utf-8'))
			self.sock_output_mutex.release()

	def on_modified(self,event):
		if not event.is_directory:
			filename = event.src_path
			modifiedfile_dict = dict()
			modifiedfile_dict['command'] = "ModFile"
			modifiedfile_dict['filename'] = filename
			modifiedfile_json = json.dumps(modifiedfile_dict)
			self.sock_output_mutex.acquire()
			self.server_sock.sendall(modifiedfile_json.encode('utf-8'))
			self.sock_output_mutex.release()

	def on_moved(self,event):
		#print("src_path = ",event.src_path," dest_path = ",event.dest_path)
		oldname = event.src_path
		newname = event.dest_path

		move_dict = dict()
		move_dict['command'] = "Move"
		move_dict['oldname'] = oldname
		move_dict['newname'] = newname
		move_json = json.dumps(move_dict)
		self.sock_output_mutex.acquire()
		self.server_sock.sendall(move_json.encode('utf-8'))
		self.sock_output_mutex.release()

class Client:
	"""docstring for client"""
	def __init__(self, arg):
		
		self.host = arg[1]
		self.port = int(arg[2])
		self.sock_output_mutex = threading.Lock()
		self.size = 2048
		self.threads = []

	def connect_server(self):
		try:
			self.server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			self.server_sock.connect( (self.host, self.port) )
		except Exception as e:
			print("error on connecting to server")
			

	
	def run(self):

		running = 1
		self.connect_server()
		recv_msg_from_server = Recv_Msg_From_Server(self.server_sock,self.sock_output_mutex)
		recv_msg_from_server.start()
		self.threads.append(recv_msg_from_server)

		if not os.path.isdir("./users"):
			os.mkdir("users",0o777)
		
		while running:
			time.sleep(1)
			cmd = input("command >")

			if cmd=="R":
				reg_dict = dict()
				username = input("username >")
				if len(username) > 50:
					print("Can't use username more than 50 characters")
					continue
				passwd1 = input("password >")
				if len(passwd1) > 50:
					print("Can't use password more than 50 characters")
					continue
				passwd2 = input("password again >")
				if(passwd1 != passwd2):
					print("different password! Try again!")
					continue

				reg_dict["command"] = "Register"
				reg_dict["username"] = username
				reg_dict["passwd"] = passwd1

				reg_json = json.dumps(reg_dict)
				
				self.server_sock.sendall(reg_json.encode('utf-8'))
				
				continue
			elif cmd == "L":
				login_dict = dict()
				username = input("username >")
				passwd = input("password >")
				login_dict["command"] = "Login"
				login_dict["username"] = username
				login_dict["passwd"] = passwd
				

				login_json = json.dumps(login_dict)
				
				self.server_sock.sendall(login_json.encode("utf-8"))
				time.sleep(1)
				
				time.sleep(1)
				userpath = "./users/" + username
				"""SYNC OPERATION HERE"""
				
				observer = Observer()
				event_handler = FileHandler(self.server_sock,self.sock_output_mutex)
				try:
					observer.schedule(event_handler, path = userpath , recursive = True)
				except Exception as e:
					continue
					
				observer.start()

				

				continue
			else:
				print("Wrong Command. Try again")
				continue
			recv_msg_from_server.join()
			#if len(data) <=0:
				#end_cli()
			#self.server_sock.sendall(data.encode('utf-8'))

			

	def end_cli(self):
		self.server_sock.close()
		sys.exit()



if __name__ == "__main__":
	c = Client(sys.argv)
	c.run()
		
